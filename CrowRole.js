const Discord = require("discord.js");
const  client = new Discord.Client();
const config = require("./config.json");

client.on("ready", () => {
   console.log("Estoy listo!");
});

var prefix = config.prefix;

client.on("message", (message) => {
    let miembro = message.mentions.members.first();

    if (!message.content.startsWith(config.prefix)) return;
    if (message.author.bot) return;

    var roleOne = message.guild.roles.cache.find(r => r.name === 'Vendedores');
    message.member.roles.remove(roleOne).catch(console.error);
    roleOne = message.guild.roles.cache.find(r => r.name === 'compradores');
    message.member.roles.remove(roleOne).catch(console.error);
    roleOne = message.guild.roles.cache.find(r => r.name === "personas");
    message.member.roles.remove(roleOne).catch(console.error);
 
  if (message.content.startsWith(prefix + "vendedores")) {
    var role = message.guild.roles.cache.find(r => r.name === 'Vendedores');
    message.member.roles.add(role).catch(console.error);
    message.channel.send({embed: {
        color: 3447003,
        description: "¡Ha sido asignado como vendedor!"
      }});

  } else
  if (message.content.startsWith(prefix + "cliente")) {
    var role = message.guild.roles.cache.find(r => r.name === 'compradores');
    message.member.roles.add(role).catch(console.error);
    message.channel.send({embed: {
        color: 3447003,
        description: "¡Ha sido asignado como comprador!"
      }});
  }
});

client.login(config.token); 